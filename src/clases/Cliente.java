package clases;

/**
 * Created by repre on 09/07/2017.
 */
public class Cliente {

    private String nombreCliente;
    private int codigoCliente;

    public Cliente() {
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }
}
