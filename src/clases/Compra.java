package clases;

/**
 * Created by repre on 11/07/2017.
 */
public class Compra {
    private String cliente;
    private String proyecto;
    private float valor;

    public Compra() {
    }

    public Compra(String cliente, String proyecto, float valor) {
        this.cliente = cliente;
        this.proyecto = proyecto;
        this.valor = valor;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
}
