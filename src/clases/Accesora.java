package clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by repre on 09/07/2017.
 */
public class Accesora {

    private Cliente clientes;
    private List<Cliente> listadoClientes;
    private Compra compras;
    private List<Compra> listadoCompras;

    public Accesora() {

        listadoClientes = new ArrayList<>();
        listadoCompras = new ArrayList<>();
    }

    public void agregarCliente(Cliente c) {
        listadoClientes.add(c);
    }

    public List<Cliente> getListadoClientes() {
        return listadoClientes;
    }

    public void agregarCompra(Compra cr){
        listadoCompras.add(cr);
    }

    public List<Compra> getListadoCompras(){
        return listadoCompras;}
}
