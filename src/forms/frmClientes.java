package forms;

import clases.Accesora;
import clases.Cliente;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by repre on 09/07/2017.
 */
public class frmClientes extends javax.swing.JFrame {

    private JPanel panel1;
    private JTextField clienteTxt;
    private JTextField codigoTxt;
    private JButton agregarBtn;
    private JTable table1;
    private JButton button1;
    private Accesora accesora;
    private DefaultTableModel model;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Clientes");
        frame.setLocationRelativeTo(null);
        frame.setContentPane(new frmClientes().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public frmClientes() {
        accesora = new Accesora();
        model = new DefaultTableModel();
        model.addColumn("Nombre cliente");
        model.addColumn("Codigo cliente");
        table1.setModel(model);

        agregarBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agregarCliente();
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Cliente c : accesora.getListadoClientes()) {
                    model.addRow(new Object[]{c.getNombreCliente(), c.getCodigoCliente()});
                }
            }
        });
    }

    private void agregarCliente() {
        Cliente cl = new Cliente();
        cl.setNombreCliente(clienteTxt.getText());
        cl.setCodigoCliente(Integer.parseInt(codigoTxt.getText()));
        accesora.agregarCliente(cl);
        clienteTxt.setText("");
        codigoTxt.setText("");
    }
}
