package forms;

import clases.Accesora;
import clases.Compra;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by repre on 11/07/2017.
 */
public class frmCompras {
    private JPanel panel1;
    private JTextField clienteTxt;
    private JTextField proyectoTxt;
    private JTextField valorTxt;
    private JButton agregarButton;
    private JButton verDatosButton;
    private JTable table1;
    private DefaultTableModel model;
    private Accesora accesora;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Compras");
        frame.setLocationRelativeTo(null);
        frame.setContentPane(new frmCompras().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public frmCompras() {
        accesora = new Accesora();
        model = new DefaultTableModel();
        model.addColumn("Cliente");
        model.addColumn("Proyecto");
        model.addColumn("Valor");
        table1.setModel(model);


        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Compra compra = new Compra();
                compra.setCliente(clienteTxt.getText());
                compra.setProyecto(proyectoTxt.getText());
                compra.setValor(Float.parseFloat(valorTxt.getText()));
                accesora.agregarCompra(compra);

                clienteTxt.setText("");
                proyectoTxt.setText("");
                valorTxt.setText("");
            }
        });
        verDatosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Compra compra:accesora.getListadoCompras()){
                    model.addRow(new Object[]{compra.getCliente(), compra.getProyecto(), compra.getValor()});
                }
            }
        });


    }
}
